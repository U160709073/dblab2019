CALL selectAllCustomers;

CALL getCustomersByCity("London");

SELECT * FROM Employees;

SELECT MAX(Salary) FROM Employees;


-- set @maxSalary = 0;                   -- it is arbitrary
CALL highestSalary(@maxSalary);

SELECT @maxSalary;


-- set @m_cnt = 0;
CALL count_gender(@m_cnt, 'M');
SELECT @m_cnt;

CALL count_gender(@f_cnt, 'F');
SELECT @f_cnt;

SELECT @m_cnt, @f_cnt;

